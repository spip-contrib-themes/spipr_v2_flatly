<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-theme_bsflatly
// Langue: fr
// Date: 07-04-2020 16:49:49
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'theme_bs4flatly_description' => 'Flat an modern',
	'theme_bs4flatly_slogan' => 'Flat an modern',
);
?>